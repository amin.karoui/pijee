package services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import services.IUserService;
import model.Reclamation;
import model.User;
@Stateless
@LocalBean
public class UserService implements IUserService {

	@PersistenceContext(unitName="CLUESJAVAEE-ejb")
	EntityManager em;
	@Override
	public User UserAuth(String user, String password) {
		System.out.println("d5al");
		TypedQuery<User> query =
				em.createQuery("SELECT u FROM User u WHERE u.login=:user AND u.password=:password ",
				User.class);
				query.setParameter("user", user);
				query.setParameter("password", password);
				User emp = null;
				try {
					
					emp = query.getSingleResult(); 
					System.out.println("d5al ll try");
				}
				catch (Exception e) { System.out.println("Erreur : " + e); }	
				System.out.println(user.toString());
				return emp;		
	}
	
	@Override
	public User findUserById(int id) {
		return em.find(User.class, id);
	}

	@Override
	public void updateuser(User user) {
		em.merge(user);
	}

}
