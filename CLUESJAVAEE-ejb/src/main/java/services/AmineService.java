package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import model.Candidature;
import model.Offer;
import model.User;

@Stateless
@LocalBean
public class AmineService implements IAmineService {

	@PersistenceContext(unitName="CLUESJAVAEE-ejb")
	private EntityManager entityManger ;
	@Override
	public String ajouterCandidature(Candidature c) {
		// TODO Auto-generated method stub
		entityManger.persist(c);
		return null;
	}
	@Override
	public String ajouterUser(User u) {
		entityManger.persist(u);
		return null;
	}
	@Override
	public User findUserByLogin(String login) {
		TypedQuery<User> query=entityManger.createQuery("Select a from User a Where a.login =:pp",User.class);
		query.setParameter("pp", login);
		return query.getSingleResult();
	}
	@Override
	public List<Offer> findAllOffers() {
		TypedQuery<Offer> query=entityManger.createQuery("SELECT e FROM Offer e ",Offer.class);
		 
		return query.getResultList();
	}
	@Override
	public void deleteCandidature(Candidature c) {
		// TODO Auto-generated method stub
		
		entityManger.remove(entityManger.contains(c) ? c : entityManger.merge(c));
		
	}
	

}
