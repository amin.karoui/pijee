package services;

import java.util.List;

import javax.ejb.Remote;

import model.Candidature;
import model.Offer;
import model.User;

@Remote
public interface IAmineService {
String ajouterCandidature(Candidature c);
String ajouterUser(User u);
User findUserByLogin(String login);
List<Offer> findAllOffers();
void deleteCandidature(Candidature c);
}
