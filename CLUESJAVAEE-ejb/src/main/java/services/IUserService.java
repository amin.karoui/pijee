package services;

import javax.ejb.Remote;

import model.Reclamation;
import model.User;

@Remote
public interface IUserService {
	
	public User UserAuth(String user, String password);
	public User findUserById(int id);
	
	public void updateuser(User user);  


}
