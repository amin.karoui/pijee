package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the RDVs database table.
 * 
 */
@Entity
@Table(name="RDVs")
@NamedQuery(name="RDV.findAll", query="SELECT r FROM RDV r")
public class RDV implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="RdvId")
	private int rdvId;

	@Column(name="ApplicantId")
	private int applicantId;

	@Column(name="DateRdv")
	private Date dateRdv;

	@Column(name="StateRdv")
	private int stateRdv;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="Applicant_UserId")
	private User user;

	public RDV() {
	}

	public int getRdvId() {
		return this.rdvId;
	}

	public void setRdvId(int rdvId) {
		this.rdvId = rdvId;
	}

	public int getApplicantId() {
		return this.applicantId;
	}

	public void setApplicantId(int applicantId) {
		this.applicantId = applicantId;
	}

	public Date getDateRdv() {
		return this.dateRdv;
	}

	public void setDateRdv(Date dateRdv) {
		this.dateRdv = dateRdv;
	}

	public int getStateRdv() {
		return this.stateRdv;
	}

	public void setStateRdv(int stateRdv) {
		this.stateRdv = stateRdv;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}