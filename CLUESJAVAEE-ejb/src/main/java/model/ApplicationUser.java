package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ApplicationUsers database table.
 * 
 */
@Entity
@Table(name="ApplicationUsers")
@NamedQuery(name="ApplicationUser.findAll", query="SELECT a FROM ApplicationUser a")
public class ApplicationUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	@Column(name="AppRole")
	private int appRole;

	@Column(name="Email")
	private String email;

	@Column(name="Password")
	private String password;

	@Column(name="Username")
	private String username;

	//bi-directional many-to-one association to Commentaire
	@OneToMany(mappedBy="applicationUser")
	private List<Commentaire> commentaires;

	//bi-directional many-to-one association to Message
	@OneToMany(mappedBy="applicationUser")
	private List<Message> messages;

	//bi-directional many-to-one association to Notification
	@OneToMany(mappedBy="applicationUser")
	private List<Notification> notifications;

	//bi-directional many-to-one association to Publication
	@OneToMany(mappedBy="applicationUser")
	private List<Publication> publications;

	//bi-directional many-to-one association to Reaction
	@OneToMany(mappedBy="applicationUser")
	private List<Reaction> reactions;

	public ApplicationUser() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAppRole() {
		return this.appRole;
	}

	public void setAppRole(int appRole) {
		this.appRole = appRole;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Commentaire> getCommentaires() {
		return this.commentaires;
	}

	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	public Commentaire addCommentaire(Commentaire commentaire) {
		getCommentaires().add(commentaire);
		commentaire.setApplicationUser(this);

		return commentaire;
	}

	public Commentaire removeCommentaire(Commentaire commentaire) {
		getCommentaires().remove(commentaire);
		commentaire.setApplicationUser(null);

		return commentaire;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Message addMessage(Message message) {
		getMessages().add(message);
		message.setApplicationUser(this);

		return message;
	}

	public Message removeMessage(Message message) {
		getMessages().remove(message);
		message.setApplicationUser(null);

		return message;
	}

	public List<Notification> getNotifications() {
		return this.notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public Notification addNotification(Notification notification) {
		getNotifications().add(notification);
		notification.setApplicationUser(this);

		return notification;
	}

	public Notification removeNotification(Notification notification) {
		getNotifications().remove(notification);
		notification.setApplicationUser(null);

		return notification;
	}

	public List<Publication> getPublications() {
		return this.publications;
	}

	public void setPublications(List<Publication> publications) {
		this.publications = publications;
	}

	public Publication addPublication(Publication publication) {
		getPublications().add(publication);
		publication.setApplicationUser(this);

		return publication;
	}

	public Publication removePublication(Publication publication) {
		getPublications().remove(publication);
		publication.setApplicationUser(null);

		return publication;
	}

	public List<Reaction> getReactions() {
		return this.reactions;
	}

	public void setReactions(List<Reaction> reactions) {
		this.reactions = reactions;
	}

	public Reaction addReaction(Reaction reaction) {
		getReactions().add(reaction);
		reaction.setApplicationUser(this);

		return reaction;
	}

	public Reaction removeReaction(Reaction reaction) {
		getReactions().remove(reaction);
		reaction.setApplicationUser(null);

		return reaction;
	}

}