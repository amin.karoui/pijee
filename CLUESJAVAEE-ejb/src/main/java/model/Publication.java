package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Publications database table.
 * 
 */
@Entity
@Table(name="Publications")
@NamedQuery(name="Publication.findAll", query="SELECT p FROM Publication p")
public class Publication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_publication")
	private int idPublication;

	@Column(name="date_publication")
	private Date datePublication;

	private String description;

	@Column(name="FilePath")
	private String filePath;

	private int nbcommentaire;

	private int nbdislike;

	private int nblike;

	@Column(name="type_publication")
	private String typePublication;

	//bi-directional many-to-one association to Commentaire
	@OneToMany(mappedBy="publication")
	private List<Commentaire> commentaires;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne
	@JoinColumn(name="UserFK")
	private ApplicationUser applicationUser;

	//bi-directional many-to-one association to Reaction
	@OneToMany(mappedBy="publication")
	private List<Reaction> reactions;

	public Publication() {
	}

	public int getIdPublication() {
		return this.idPublication;
	}

	public void setIdPublication(int idPublication) {
		this.idPublication = idPublication;
	}

	public Date getDatePublication() {
		return this.datePublication;
	}

	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getNbcommentaire() {
		return this.nbcommentaire;
	}

	public void setNbcommentaire(int nbcommentaire) {
		this.nbcommentaire = nbcommentaire;
	}

	public int getNbdislike() {
		return this.nbdislike;
	}

	public void setNbdislike(int nbdislike) {
		this.nbdislike = nbdislike;
	}

	public int getNblike() {
		return this.nblike;
	}

	public void setNblike(int nblike) {
		this.nblike = nblike;
	}

	public String getTypePublication() {
		return this.typePublication;
	}

	public void setTypePublication(String typePublication) {
		this.typePublication = typePublication;
	}

	public List<Commentaire> getCommentaires() {
		return this.commentaires;
	}

	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	public Commentaire addCommentaire(Commentaire commentaire) {
		getCommentaires().add(commentaire);
		commentaire.setPublication(this);

		return commentaire;
	}

	public Commentaire removeCommentaire(Commentaire commentaire) {
		getCommentaires().remove(commentaire);
		commentaire.setPublication(null);

		return commentaire;
	}

	public ApplicationUser getApplicationUser() {
		return this.applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public List<Reaction> getReactions() {
		return this.reactions;
	}

	public void setReactions(List<Reaction> reactions) {
		this.reactions = reactions;
	}

	public Reaction addReaction(Reaction reaction) {
		getReactions().add(reaction);
		reaction.setPublication(this);

		return reaction;
	}

	public Reaction removeReaction(Reaction reaction) {
		getReactions().remove(reaction);
		reaction.setPublication(null);

		return reaction;
	}

}