package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Reactions database table.
 * 
 */
@Entity
@Table(name="Reactions")
@NamedQuery(name="Reaction.findAll", query="SELECT r FROM Reaction r")
public class Reaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reaction")
	private int idReaction;

	@Column(name="type_reaction")
	private int typeReaction;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne
	@JoinColumn(name="user_Id")
	private ApplicationUser applicationUser;

	//bi-directional many-to-one association to Commentaire
	@ManyToOne
	private Commentaire commentaire;

	//bi-directional many-to-one association to Publication
	@ManyToOne
	private Publication publication;

	public Reaction() {
	}

	public int getIdReaction() {
		return this.idReaction;
	}

	public void setIdReaction(int idReaction) {
		this.idReaction = idReaction;
	}

	public int getTypeReaction() {
		return this.typeReaction;
	}

	public void setTypeReaction(int typeReaction) {
		this.typeReaction = typeReaction;
	}

	public ApplicationUser getApplicationUser() {
		return this.applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public Commentaire getCommentaire() {
		return this.commentaire;
	}

	public void setCommentaire(Commentaire commentaire) {
		this.commentaire = commentaire;
	}

	public Publication getPublication() {
		return this.publication;
	}

	public void setPublication(Publication publication) {
		this.publication = publication;
	}

}