package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the Reclamations database table.
 * 
 */
@Entity
@Table(name="Reclamations")
@NamedQuery(name="Reclamation.findAll", query="SELECT r FROM Reclamation r")
public class Reclamation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="QuizID")
	private int quizID;

	private String desc;

	@Column(name="JobOfferID")
	private int jobOfferID;

	private String objet;

	private Date recDate;

	private Date repDate;

	private String reponse;

	private int statu;

	private int type;

	private int userID;

	//bi-directional many-to-one association to Offer
	@ManyToOne
	@JoinColumn(name="offerID_offerId")
	private Offer offer;

	public Reclamation() {
	}

	public int getQuizID() {
		return this.quizID;
	}

	public void setQuizID(int quizID) {
		this.quizID = quizID;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getJobOfferID() {
		return this.jobOfferID;
	}

	public void setJobOfferID(int jobOfferID) {
		this.jobOfferID = jobOfferID;
	}

	public String getObjet() {
		return this.objet;
	}

	public void setObjet(String objet) {
		this.objet = objet;
	}

	public Date getRecDate() {
		return this.recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public Date getRepDate() {
		return this.repDate;
	}

	public void setRepDate(Date repDate) {
		this.repDate = repDate;
	}

	public String getReponse() {
		return this.reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	public int getStatu() {
		return this.statu;
	}

	public void setStatu(int statu) {
		this.statu = statu;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getUserID() {
		return this.userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public Offer getOffer() {
		return this.offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

}