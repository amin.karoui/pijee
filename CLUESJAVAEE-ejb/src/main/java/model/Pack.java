package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Packs database table.
 * 
 */
@Entity
@Table(name="Packs")
@NamedQuery(name="Pack.findAll", query="SELECT p FROM Pack p")
public class Pack implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PackID")
	private int packID;

	private String descpack;

	private int entrepriseID;

	private float price;

	private int purchasenum;

	//bi-directional many-to-one association to PackRate
	@OneToMany(mappedBy="pack")
	private List<PackRate> packRates;

	public Pack() {
	}

	public int getPackID() {
		return this.packID;
	}

	public void setPackID(int packID) {
		this.packID = packID;
	}

	public String getDescpack() {
		return this.descpack;
	}

	public void setDescpack(String descpack) {
		this.descpack = descpack;
	}

	public int getEntrepriseID() {
		return this.entrepriseID;
	}

	public void setEntrepriseID(int entrepriseID) {
		this.entrepriseID = entrepriseID;
	}

	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getPurchasenum() {
		return this.purchasenum;
	}

	public void setPurchasenum(int purchasenum) {
		this.purchasenum = purchasenum;
	}

	public List<PackRate> getPackRates() {
		return this.packRates;
	}

	public void setPackRates(List<PackRate> packRates) {
		this.packRates = packRates;
	}

	public PackRate addPackRate(PackRate packRate) {
		getPackRates().add(packRate);
		packRate.setPack(this);

		return packRate;
	}

	public PackRate removePackRate(PackRate packRate) {
		getPackRates().remove(packRate);
		packRate.setPack(null);

		return packRate;
	}

}