package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Commentaires database table.
 * 
 */
@Entity
@Table(name="Commentaires")
@NamedQuery(name="Commentaire.findAll", query="SELECT c FROM Commentaire c")
public class Commentaire implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_commentaire")
	private int idCommentaire;

	private Date datecomment;

	private String description;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne
	@JoinColumn(name="user_Id")
	private ApplicationUser applicationUser;

	//bi-directional many-to-one association to Publication
	@ManyToOne
	@JoinColumn(name="PublicationFK")
	private Publication publication;

	//bi-directional many-to-one association to Reaction
	@OneToMany(mappedBy="commentaire")
	private List<Reaction> reactions;

	public Commentaire() {
	}

	public int getIdCommentaire() {
		return this.idCommentaire;
	}

	public void setIdCommentaire(int idCommentaire) {
		this.idCommentaire = idCommentaire;
	}

	public Date getDatecomment() {
		return this.datecomment;
	}

	public void setDatecomment(Date datecomment) {
		this.datecomment = datecomment;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ApplicationUser getApplicationUser() {
		return this.applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public Publication getPublication() {
		return this.publication;
	}

	public void setPublication(Publication publication) {
		this.publication = publication;
	}

	public List<Reaction> getReactions() {
		return this.reactions;
	}

	public void setReactions(List<Reaction> reactions) {
		this.reactions = reactions;
	}

	public Reaction addReaction(Reaction reaction) {
		getReactions().add(reaction);
		reaction.setCommentaire(this);

		return reaction;
	}

	public Reaction removeReaction(Reaction reaction) {
		getReactions().remove(reaction);
		reaction.setCommentaire(null);

		return reaction;
	}

}