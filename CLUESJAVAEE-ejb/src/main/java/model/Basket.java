package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Baskets database table.
 * 
 */
@Entity
@Table(name="Baskets")
@NamedQuery(name="Basket.findAll", query="SELECT b FROM Basket b")
public class Basket implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int basketID;

	private int packID;

	@Column(name="Totalprice")
	private float totalprice;

	private int userID;

	public Basket() {
	}

	public int getBasketID() {
		return this.basketID;
	}

	public void setBasketID(int basketID) {
		this.basketID = basketID;
	}

	public int getPackID() {
		return this.packID;
	}

	public void setPackID(int packID) {
		this.packID = packID;
	}

	public float getTotalprice() {
		return this.totalprice;
	}

	public void setTotalprice(float totalprice) {
		this.totalprice = totalprice;
	}

	public int getUserID() {
		return this.userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

}