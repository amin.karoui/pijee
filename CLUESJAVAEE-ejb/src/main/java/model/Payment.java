package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the Payments database table.
 * 
 */
@Entity
@Table(name="Payments")
@NamedQuery(name="Payment.findAll", query="SELECT p FROM Payment p")
public class Payment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PaiementID")
	private int paiementID;

	@Column(name="InterviewType")
	private String interviewType;

	@Column(name="JobOfferID")
	private int jobOfferID;

	private int packID;

	@Column(name="PaiementDate")
	private Date paiementDate;

	@Column(name="PaiementType")
	private String paiementType;

	private int userID;

	public Payment() {
	}

	public int getPaiementID() {
		return this.paiementID;
	}

	public void setPaiementID(int paiementID) {
		this.paiementID = paiementID;
	}

	public String getInterviewType() {
		return this.interviewType;
	}

	public void setInterviewType(String interviewType) {
		this.interviewType = interviewType;
	}

	public int getJobOfferID() {
		return this.jobOfferID;
	}

	public void setJobOfferID(int jobOfferID) {
		this.jobOfferID = jobOfferID;
	}

	public int getPackID() {
		return this.packID;
	}

	public void setPackID(int packID) {
		this.packID = packID;
	}

	public Date getPaiementDate() {
		return this.paiementDate;
	}

	public void setPaiementDate(Date paiementDate) {
		this.paiementDate = paiementDate;
	}

	public String getPaiementType() {
		return this.paiementType;
	}

	public void setPaiementType(String paiementType) {
		this.paiementType = paiementType;
	}

	public int getUserID() {
		return this.userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

}