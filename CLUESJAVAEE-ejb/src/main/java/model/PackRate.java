package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PackRates database table.
 * 
 */
@Entity
@Table(name="PackRates")
@NamedQuery(name="PackRate.findAll", query="SELECT p FROM PackRate p")
public class PackRate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	private String comment;

	private int rate;

	private int userID;

	//bi-directional many-to-one association to Pack
	@ManyToOne
	@JoinColumn(name="packID")
	private Pack pack;

	public PackRate() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getRate() {
		return this.rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public int getUserID() {
		return this.userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public Pack getPack() {
		return this.pack;
	}

	public void setPack(Pack pack) {
		this.pack = pack;
	}

}