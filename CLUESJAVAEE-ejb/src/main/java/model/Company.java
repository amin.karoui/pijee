package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Companies database table.
 * 
 */
@Entity
@Table(name="Companies")
@NamedQuery(name="Company.findAll", query="SELECT c FROM Company c")
public class Company implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ComapnyID")
	private int comapnyID;

	private int chefID;

	@Column(name="date_creation")
	private Date dateCreation;

	private String nom;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="Candidat_UserId")
	private User user;

	//bi-directional many-to-one association to Subscribe
	@OneToMany(mappedBy="company")
	private List<Subscribe> subscribes;

	public Company() {
	}

	public int getComapnyID() {
		return this.comapnyID;
	}

	public void setComapnyID(int comapnyID) {
		this.comapnyID = comapnyID;
	}

	public int getChefID() {
		return this.chefID;
	}

	public void setChefID(int chefID) {
		this.chefID = chefID;
	}

	public Date getDateCreation() {
		return this.dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Subscribe> getSubscribes() {
		return this.subscribes;
	}

	public void setSubscribes(List<Subscribe> subscribes) {
		this.subscribes = subscribes;
	}

	public Subscribe addSubscribe(Subscribe subscribe) {
		getSubscribes().add(subscribe);
		subscribe.setCompany(this);

		return subscribe;
	}

	public Subscribe removeSubscribe(Subscribe subscribe) {
		getSubscribes().remove(subscribe);
		subscribe.setCompany(null);

		return subscribe;
	}

}