package model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Users database table.
 * 
 */
@Entity
@Table(name="Users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="UserId")
	private int userId;

	private String actualPost;

	private String address;

	@Column(name="applicant_state")
	private int applicantState;

	private String bio;

	private Date birthDate;

	@Column(name="Country")
	private String country;

	@Column(name="Discriminator")
	private String discriminator;

	private String experience;

	private String lastname;

	private String login;

	@Column(name="Mail")
	private String mail;

	private String name;

	private String password;

	private long phoneContact;

	private String picture;

	@Column(name="Skills")
	private String skills;

	//bi-directional many-to-one association to ApplicantAnswer
	@OneToMany(mappedBy="user")
	private List<ApplicantAnswer> applicantAnswers;

	//bi-directional many-to-one association to ApplicantRequest
	@OneToMany(mappedBy="user")
	private List<ApplicantRequest> applicantRequests;

	//bi-directional many-to-one association to Candidature
	@OneToMany(mappedBy="user",fetch=FetchType.EAGER)
	private List<Candidature> candidatures;

	//bi-directional many-to-one association to Company
	@OneToMany(mappedBy="user")
	private List<Company> companies;

	//bi-directional many-to-one association to Contact
	@OneToMany(mappedBy="user")
	private List<Contact> contacts;

	//bi-directional many-to-one association to Question
	@OneToMany(mappedBy="user")
	private List<Question> questions;

	//bi-directional many-to-one association to RDV
	@OneToMany(mappedBy="user")
	private List<RDV> rdvs;

	//bi-directional many-to-one association to Subscribe
	@OneToMany(mappedBy="user")
	private List<Subscribe> subscribes;

	public User() {
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getActualPost() {
		return this.actualPost;
	}

	public void setActualPost(String actualPost) {
		this.actualPost = actualPost;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getApplicantState() {
		return this.applicantState;
	}

	public void setApplicantState(int applicantState) {
		this.applicantState = applicantState;
	}

	public String getBio() {
		return this.bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDiscriminator() {
		return this.discriminator;
	}

	public void setDiscriminator(String discriminator) {
		this.discriminator = discriminator;
	}

	public String getExperience() {
		return this.experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getPhoneContact() {
		return this.phoneContact;
	}

	public void setPhoneContact(long phoneContact) {
		this.phoneContact = phoneContact;
	}

	public String getPicture() {
		return this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getSkills() {
		return this.skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public List<ApplicantAnswer> getApplicantAnswers() {
		return this.applicantAnswers;
	}

	public void setApplicantAnswers(List<ApplicantAnswer> applicantAnswers) {
		this.applicantAnswers = applicantAnswers;
	}

	public ApplicantAnswer addApplicantAnswer(ApplicantAnswer applicantAnswer) {
		getApplicantAnswers().add(applicantAnswer);
		applicantAnswer.setUser(this);

		return applicantAnswer;
	}

	public ApplicantAnswer removeApplicantAnswer(ApplicantAnswer applicantAnswer) {
		getApplicantAnswers().remove(applicantAnswer);
		applicantAnswer.setUser(null);

		return applicantAnswer;
	}

	public List<ApplicantRequest> getApplicantRequests() {
		return this.applicantRequests;
	}

	public void setApplicantRequests(List<ApplicantRequest> applicantRequests) {
		this.applicantRequests = applicantRequests;
	}

	public ApplicantRequest addApplicantRequest(ApplicantRequest applicantRequest) {
		getApplicantRequests().add(applicantRequest);
		applicantRequest.setUser(this);

		return applicantRequest;
	}

	public ApplicantRequest removeApplicantRequest(ApplicantRequest applicantRequest) {
		getApplicantRequests().remove(applicantRequest);
		applicantRequest.setUser(null);

		return applicantRequest;
	}

	public List<Candidature> getCandidatures() {
		return this.candidatures;
	}

	public void setCandidatures(List<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public Candidature addCandidature(Candidature candidature) {
		getCandidatures().add(candidature);
		candidature.setUser(this);

		return candidature;
	}

	public Candidature removeCandidature(Candidature candidature) {
		getCandidatures().remove(candidature);
		candidature.setUser(null);

		return candidature;
	}

	public List<Company> getCompanies() {
		return this.companies;
	}

	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	public Company addCompany(Company company) {
		getCompanies().add(company);
		company.setUser(this);

		return company;
	}

	public Company removeCompany(Company company) {
		getCompanies().remove(company);
		company.setUser(null);

		return company;
	}

	public List<Contact> getContacts() {
		return this.contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public Contact addContact(Contact contact) {
		getContacts().add(contact);
		contact.setUser(this);

		return contact;
	}

	public Contact removeContact(Contact contact) {
		getContacts().remove(contact);
		contact.setUser(null);

		return contact;
	}

	public List<Question> getQuestions() {
		return this.questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Question addQuestion(Question question) {
		getQuestions().add(question);
		question.setUser(this);

		return question;
	}

	public Question removeQuestion(Question question) {
		getQuestions().remove(question);
		question.setUser(null);

		return question;
	}

	public List<RDV> getRdvs() {
		return this.rdvs;
	}

	public void setRdvs(List<RDV> rdvs) {
		this.rdvs = rdvs;
	}

	public RDV addRdv(RDV rdv) {
		getRdvs().add(rdv);
		rdv.setUser(this);

		return rdv;
	}

	public RDV removeRdv(RDV rdv) {
		getRdvs().remove(rdv);
		rdv.setUser(null);

		return rdv;
	}

	public List<Subscribe> getSubscribes() {
		return this.subscribes;
	}

	public void setSubscribes(List<Subscribe> subscribes) {
		this.subscribes = subscribes;
	}

	public Subscribe addSubscribe(Subscribe subscribe) {
		getSubscribes().add(subscribe);
		subscribe.setUser(this);

		return subscribe;
	}

	public Subscribe removeSubscribe(Subscribe subscribe) {
		getSubscribes().remove(subscribe);
		subscribe.setUser(null);

		return subscribe;
	}

	public User(int userId, String actualPost, String address, int applicantState, String bio, Date birthDate,
			String country, String discriminator, String experience, String lastname, String login, String mail,
			String name, String password, long phoneContact, String picture, String skills,
			List<ApplicantAnswer> applicantAnswers, List<ApplicantRequest> applicantRequests,
			List<Candidature> candidatures, List<Company> companies, List<Contact> contacts, List<Question> questions,
			List<RDV> rdvs, List<Subscribe> subscribes) {
		super();
		this.userId = userId;
		this.actualPost = actualPost;
		this.address = address;
		this.applicantState = applicantState;
		this.bio = bio;
		this.birthDate = birthDate;
		this.country = country;
		this.discriminator = discriminator;
		this.experience = experience;
		this.lastname = lastname;
		this.login = login;
		this.mail = mail;
		this.name = name;
		this.password = password;
		this.phoneContact = phoneContact;
		this.picture = picture;
		this.skills = skills;
		this.applicantAnswers = applicantAnswers;
		this.applicantRequests = applicantRequests;
		this.candidatures = candidatures;
		this.companies = companies;
		this.contacts = contacts;
		this.questions = questions;
		this.rdvs = rdvs;
		this.subscribes = subscribes;
	}

	public User(String actualPost, String address, int applicantState, String bio, Date birthDate, String country,
			String discriminator, String experience, String lastname, String login, String mail, String name,
			String password, long phoneContact, String picture, String skills, List<ApplicantAnswer> applicantAnswers,
			List<ApplicantRequest> applicantRequests, List<Candidature> candidatures, List<Company> companies,
			List<Contact> contacts, List<Question> questions, List<RDV> rdvs, List<Subscribe> subscribes) {
		super();
		this.actualPost = actualPost;
		this.address = address;
		this.applicantState = applicantState;
		this.bio = bio;
		this.birthDate = birthDate;
		this.country = country;
		this.discriminator = discriminator;
		this.experience = experience;
		this.lastname = lastname;
		this.login = login;
		this.mail = mail;
		this.name = name;
		this.password = password;
		this.phoneContact = phoneContact;
		this.picture = picture;
		this.skills = skills;
		this.applicantAnswers = applicantAnswers;
		this.applicantRequests = applicantRequests;
		this.candidatures = candidatures;
		this.companies = companies;
		this.contacts = contacts;
		this.questions = questions;
		this.rdvs = rdvs;
		this.subscribes = subscribes;
	}
	

}