package beansjsf;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Candidature;
import model.Offer;
import model.User;
import services.AmineService;
import services.IUserService;
import services.UserService;

@SessionScoped
@ManagedBean
public class AmineBean {
@EJB
AmineService am;
@EJB
IUserService us= new UserService();
private List<Offer> offers;
private List<Offer> offersCandidatures;

private int candidatureId;
private String etat;
private Offer offer;
private User user;

private int userId;
private String actualPost;

private String address;
private int applicantState;
private String bio;
private Date birthDate;
private String country;
private String discriminator;
private String experience;
private String lastname;
private String login;
private String mail;
private String name;
private String password;
private long phoneContact;
private String picture;
private String skills;

private List<String> selectNames=new ArrayList<String>();
private String location="";
private static List<Offer> rechercheOffer;

public List<Offer> getRechercheOffer() {
	return rechercheOffer;
}
public void setRechercheOffer(List<Offer> rechercheOffer) {
	this.rechercheOffer = rechercheOffer;
}
public List<String> getSelectNames() {
	return selectNames;
}
public void setSelectNames(List<String> selectNames) {
	this.selectNames = selectNames;
}
public String getLocation() {
	return location;
}
public void setLocation(String location) {
	this.location = location;
}

private static User userCo;

public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getDiscriminator() {
	return discriminator;
}
public void setDiscriminator(String discriminator) {
	this.discriminator = discriminator;
}
public String getExperience() {
	return experience;
}
public void setExperience(String experience) {
	this.experience = experience;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getLogin() {
	return login;
}
public void setLogin(String login) {
	this.login = login;
}
public String getMail() {
	return mail;
}
public void setMail(String mail) {
	this.mail = mail;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public long getPhoneContact() {
	return phoneContact;
}
public void setPhoneContact(long phoneContact) {
	this.phoneContact = phoneContact;
}
public String getPicture() {
	return picture;
}
public void setPicture(String picture) {
	this.picture = picture;
}
public String getSkills() {
	return userCo.getLogin();
}
public void setSkills(String skills) {
	this.skills = skills;
}
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public String getActualPost() {
	return actualPost;
}
public void setActualPost(String actualPost) {
	this.actualPost = actualPost;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public int getApplicantState() {
	return applicantState;
}
public void setApplicantState(int applicantState) {
	this.applicantState = applicantState;
}
public String getBio() {
	return bio;
}
public void setBio(String bio) {
	this.bio = bio;
}
public Date getBirthDate() {
	return birthDate;
}
public void setBirthDate(Date birthDate) {
	this.birthDate = birthDate;
}
public AmineService getAm() {
	return am;
}
public void setAm(AmineService am) {
	this.am = am;
}
public int getCandidatureId() {
	return candidatureId;
}
public void setCandidatureId(int candidatureId) {
	this.candidatureId = candidatureId;
}
public String getEtat() {
	return etat;
}
public void setEtat(String etat) {
	this.etat = etat;
}
public Offer getOffer() {
	return offer;
}
public void setOffer(Offer offer) {
	this.offer = offer;
}
public User getUser() {
	return user;
}
public void setUser(User user) {
	this.user = user;
}
public List<Offer> getOffers() {
	Boolean i = true;
	List<Offer> oofersNon = new ArrayList<Offer>();
	for(Offer o1 :am.findAllOffers() )
	{
		i=false;
		for(Candidature c :us.findUserById(loginBean.userID).getCandidatures())
	{
		
		if (c.getOffer().getOfferId()==o1.getOfferId())
			i=true;
		
	}
	
	if(i==false)
		oofersNon.add(o1);
	
	}
	return oofersNon;
}
public void setOffers(List<Offer> offers) {
	this.offers = offers;
}

public List<Offer> getOffersCandidatures() {
	List<Offer> oofersOui = new ArrayList<Offer>();
	for(Offer o1 :am.findAllOffers() )
	{
		for(Candidature c :us.findUserById(loginBean.userID).getCandidatures())
	{
		
		if (c.getOffer().getOfferId()==o1.getOfferId())
			oofersOui.add(o1);
	}
	
		
	
	}
	return oofersOui;}
public void setOffersCandidatures(List<Offer> offersCandidatures) {
	this.offersCandidatures = offersCandidatures;
}
//***************************
 


public String ajouterUser(User u)
{
	am.ajouterUser(u);
	return "ajou";
		
	
}
public User findUser(String login )
{
	return am.findUserByLogin(login);
	
}


public static User getUserCo() {
	return userCo;
}
public static void setUserCo(User userCo) {
	AmineBean.userCo = userCo;
}
public String signIn() { 
	
	User u =findUser(login) ;
	String navigateTo = "null";
	if(u!=null) {
		if(u.getPassword().equals(password))
			{userCo=u;
			 navigateTo = "/template/addUser?faces-redirect=true";
			

			
			}
		else 
			return null ;
		
	}
	else return null ;
	 return navigateTo;
	 
}
public String Postuler(Offer o)
{
	Candidature c =new Candidature();
	c.setOffer(o);
	c.setEtat("en cours");
	c.setUser(us.findUserById(loginBean.userID));
	am.ajouterCandidature(c);
	return "/templatefront/candidatures.jsf?faces-redirect=true";
}
public void Supprimer(Offer o)
{
	for(Candidature c :us.findUserById(loginBean.userID).getCandidatures())
	{
		
		if (c.getOffer().getOfferId()==o.getOfferId())
			am.deleteCandidature(c);
	}	
}
public float Match(Offer o)
{
	List<String>competences = new ArrayList<String>();
	String mot=",";
	int b=1;
	float d=0;
	for(char c:o.getReferenceOffer().toCharArray())
	{
		if (c==',')
			b++;
	}
	/*for(char c1:us.findUserById(loginBean.userID).getExperience().toCharArray())
	{
		if(c1!=',')
		mot=mot+c1;
		if(c1==',')
		{competences.add(mot);
		mot="";}
	}*/
	String mots2[]=o.getReferenceOffer().split(mot);

	String mots[]=us.findUserById(loginBean.userID).getExperience().split(mot);
	for(int i=0;i<mots.length;i++)
	{
		for(int j=0;j<mots2.length;j++)
		{
			if(mots[i].equalsIgnoreCase(mots2[j]))
			d++;
		}
	}	
	float m=(d/(float)mots2.length);
	return (Math.round(m*100));
}

public String Search()
{
	List<Offer> allOffers		=	am.findAllOffers();
	List<Offer> offerNames		=	new ArrayList<Offer>();
	List<Offer> offerLocations	=	new ArrayList<Offer>();
	List<Offer> offerResult		=	new ArrayList<Offer>();
	List<Offer> offerSubResult	=	new ArrayList<Offer>();
	for(Offer o:allOffers)
	{
		for(String name:selectNames)
		{
			if(o.getTitleOffer().equalsIgnoreCase(name))
			{
				offerNames.add(o);
			}
		}
	}
	for(Offer o:allOffers)
	{
		if(o.getLocation().equalsIgnoreCase(location))
		{
			offerLocations.add(o);
		}
	}
	if(offerNames.isEmpty())
	{
		offerResult	=	offerLocations;
	}
	else if(offerLocations.isEmpty())
	{
		offerResult	=	offerNames;
	}
	else if((!offerNames.isEmpty())&&(!offerLocations.isEmpty()))
	{
		offerResult	=	offerNames;
		for(Offer oLocations:offerLocations)
		{
			for(Offer o:offerResult)
			{
				if(o.getOfferId()!=oLocations.getOfferId())
				{
					offerSubResult.add(oLocations);
				}
			}
		}
	}
	for(Offer sub:offerSubResult)
	{
		offerResult.add(sub);
	}
	if(offerResult.isEmpty())
	{
		rechercheOffer	=	allOffers;
	}
	else
	{
		rechercheOffer	=	offerResult;
	}
	return "/templatefront/offerRecherche.jsf?faces-redirect=true";
}












}
