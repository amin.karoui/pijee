package beansjsf;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import services.IUserService;
import model.User;
import services.UserService;

@ManagedBean(name= "loginBean") //bech ya9raha jsf
@SessionScoped //visible
public class loginBean {
	@EJB
	
	IUserService us= new UserService();
	
	private String username;
	private String password;
	
	public static int userID;
	
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	 
	public String doLogin() {
		System.out.println(username);
		
		   User uti = us.UserAuth(username, password);
	       FacesContext context = FacesContext.getCurrentInstance();

	       if (uti == null) {
	           context.addMessage(null, new FacesMessage("Unknown login, try again"));
	           username = null;
	           password = null;
	           return null;
	       } else if(uti.getApplicantState()==1) {
	           context.getExternalContext().getSessionMap().put("user", uti);
	           userID=uti.getUserId();
	           return "templatefront/offer.jsf?faces-redirect=true";
	       }else if(uti.getApplicantState()==3) {
	           context.getExternalContext().getSessionMap().put("user", uti);
	           userID=uti.getUserId();
	           return "/login.jsf?faces-redirect=true";
	       }
	       
	       else {
	    	   context.getExternalContext().getSessionMap().put("user", uti);
	    	   userID=uti.getUserId();
	           return "Reclamation/listReclamation.jsf?faces-redirect=true";
	    	   
	       }
		
	

}
}
